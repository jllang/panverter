# Changelog for Panverter

This is the changelog of Panverter. Some important changes prior version 0.1.5.0
are unfortunately undocumented.

## Unreleased changes
- Currently no unrealeased changes

## 0.1.6.0
### Additions
- Added support for SQLite relational database output files.

## 0.1.5.0
### Changes
- The project was renamed from Spin2Latex to Panverter
- The LaTeX output functionality now produces immediately compilable LaTeX code
  that can be used as a standalone tex file or included in another

### Additions
- As a redundant copy of the information shown at GitLab, this changelog was
  started

## 0.1.4.1
### Changes
- The HTML output produced by Spin2Latex now passes the W3C HTML validator

### Additions
- HTML output now supports stylesheets via the `--stylesheet` and `-s` options

## 0.1.4.0
### Additions
- Fourteen new fields/columns
- An option for (over)writing the output into a file instead of `stdout`

## 0.1.3.0
### Additions
- Added support for Haskell `Show`/`Read` style output
- Permutations for token words that determine the ordering of fields/columns in
  the output.

## 0.1.2.1
### Fixes
- Fixed a bug in markdown output

## 0.1.2.0
### Additions
- Spin2Latex now supports all the six promised output formats: LaTeX, HTML,
  markdown, CSV, JSON, and XML. The XML Document Type Definition can be found in
  `spin-results.dtd`

## 0.1.1.0
### Additions
- Support for the markdown output format

## 0.1.0.0
### Additions
- The initial release
- Support for output from pan and GNU Time. It scrapes the following numbers:
  - errors (pan)
  - states, stored (pan)
  - transitions (pan)
  - wall clock time (GNU Time)
  - maximum resident size (GNU Time)
- The tool goes through every file in a directory, assuming that they contain
  output from pan. The user should collect all data into a dedicated directory,
  each run of the verifier executable of Spin model checker recorded in a
  separate file. The directory `testdata` is a n example of this arrangement
  containing example files for testing the tool. The output can be generated as
  a LaTeX or HTML table (the `--help` command lists also XML, JSON, and CSV, but
  those formats aren't supported yet) in the standard output stream.
