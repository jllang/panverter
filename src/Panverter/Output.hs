module Panverter.Output (output) where

import Prelude hiding (concat, null)
import qualified Data.List.NonEmpty as N
import Data.Text.Lazy hiding (cycle, length, map, repeat, take, zip)
import Text.Printf

import Panverter.Datatypes
import Panverter.Model
import Panverter.Token
import Panverter.Utilities

length' :: Headings -> Int
length' (Headings hs) = length hs

getHeading :: Format -> Measurement -> Text
getHeading f (Measurement _ _ t) = columnName f t

getHeadings :: Format -> HideModelName -> Model -> Headings
getHeadings XML    _ (Model _ xs) =
  Headings $ N.map (getHeading XML) xs
getHeadings SQLite _ (Model _ xs) =
  Headings $ N.map (getHeading SQLite) xs
getHeadings f      (HideModelName q) (Model _ xs)
  | q         = Headings rest
  | otherwise = Headings (pack "Model"  N.:| (y:ys))
  where rest@(y N.:| ys) = N.map (getHeading f) xs

formatter :: NumberFormat -> String
formatter Fixed    = "%.3f"
formatter Floating = "%g"

formatToken :: ValueType
            -> StaticOptions
            -> Denominator
            -> Maybe Text
            -> Text
formatToken Boolean (StaticOptions _ Fixed) _ (Just t)
  | t == empty = pack "no"
  | otherwise  = pack "yes"
formatToken Boolean (StaticOptions _ Floating) _ (Just t)
  | t == empty = pack "false"
  | otherwise  = pack "true"
formatToken Boolean (StaticOptions _ Fixed) _ Nothing = pack "no"
formatToken Boolean (StaticOptions _ Floating) _ Nothing = pack "false"
formatToken Identifier  (StaticOptions _ Fixed) _ (Just t) = t
formatToken Identifier  (StaticOptions _ Floating) _ (Just t) = quote t
formatToken FixedNumber (StaticOptions (MissingValue x) _) _ (Just t)
  | t == empty = x
  | otherwise  = t
formatToken FloatingNumber (StaticOptions (MissingValue x) f) (Denominator d) (Just t)
  | t == empty = x
  | otherwise  = pack (printf (formatter f) (read (unpack t) / d))
formatToken Megabytes (StaticOptions (MissingValue x) _) _ (Just t)
  | t == empty = x
  | otherwise  = pack (printf (formatter Fixed) (read (unpack t) :: Double))
formatToken Percentage (StaticOptions (MissingValue x) _) _ (Just t)
  | t == empty = x
  | otherwise  = pack (printf "%.2f" (read (unpack t) :: Double))
formatToken Time (StaticOptions (MissingValue x) Fixed) _ (Just t)
  | t == empty = x
  | otherwise  = t
formatToken Time (StaticOptions (MissingValue x) Floating) _ (Just t)
  | t == empty = x
  | otherwise  = quote t
formatToken _ (StaticOptions (MissingValue x) _) _ _ = x

formatHeader :: Delimiters -> Headings -> Text
formatHeader (Delimiters s t u) (Headings hs) = s <> intercalate' t hs <> u

formatContent :: StaticOptions
              -> DynamicOptions
              -> Delimiters
              -> (Name -> Text)
              -> Models
              -> Text
formatContent x@(StaticOptions _ _)
              (DynamicOptions (HideModelName b) d _ _)
              (Delimiters s t u)
              g
              rs =
  let
    h (Measurement mt t' _) = formatToken t' x d mt
    k n = if b then empty else g n <> t
    row (Model n xs) = s <> k n <> intercalate' t (N.map h xs) <> u
  in
    foldMap row rs

indent  :: Indentation -> Int -> Text
indent  (Indentation i) n = pack (take (n * i)     $ repeat ' ')

missingValue :: MissingValue
missingValue = MissingValue $ pack "?"

linebreak :: Text
linebreak = pack "\n"

times :: Int -> String -> Text
times n t = pack $ take (n * length t) (cycle t)

quotationMark :: Text
quotationMark = pack "\""

quote :: Text -> Text
quote t = quotationMark <> t <> quotationMark

--------------------------------------------------------------------------------

csvDelimiters :: Delimiters
csvDelimiters = Delimiters empty (pack ";") linebreak

csvOptions :: StaticOptions
csvOptions = StaticOptions missingValue Floating

csv :: OutputProcessor
csv o@(DynamicOptions q _ _ _) rs@(r N.:| _) =
  formatHeader csvDelimiters (getHeadings CSV q r) <>
  formatContent csvOptions o csvDelimiters (quote . getName) rs

--------------------------------------------------------------------------------

thDelimiters :: Indentation -> Delimiters
thDelimiters i = Delimiters
               (indent i 3 <> pack "<tr>\n" <> indent i 4 <> pack "<th>")
               (pack "</th>\n" <> indent i 4 <> pack "<th>")
               (pack "</th>\n" <> indent i 3 <> pack "</tr>\n")

tdDelimiters :: Indentation -> Delimiters
tdDelimiters i = Delimiters
               (indent i 3 <> pack "<tr>\n" <> indent i 4 <> pack "<td>")
               (pack "</td>\n" <> indent i 4 <> pack "<td>")
               (pack "</td>\n" <> indent i 3 <> pack "</tr>\n")

htmlOptions :: StaticOptions
htmlOptions = StaticOptions missingValue Fixed

linkStylesheet :: DynamicOptions -> Text
linkStylesheet (DynamicOptions _ _ ms i) =
  let
    helper (Stylesheet s) = indent i 2 <>
      pack "<link rel=\"stylesheet\" href=\"" <> pack s <> pack "\" />\n"
  in
    maybe empty helper ms

html :: OutputProcessor
html o@(DynamicOptions q _ _ i) rs@(r N.:| _) =
  pack "<!DOCTYPE html>\n<html lang=\"en\">\n" <>
  indent i 1 <> pack "<head>\n" <>
  indent i 2 <> pack "<meta charset=\"UTF-8\">\n" <>
  linkStylesheet o <>
  indent i 2 <> pack "<title>Results</title>\n" <>
  indent i 1 <> pack "</head>\n" <>
  indent i 1 <> pack "<body>\n" <>
  indent i 2 <> pack "<table>\n" <>
  formatHeader  (thDelimiters i) (getHeadings HTML q r) <>
  formatContent (htmlOptions) o (tdDelimiters i) getName rs <>
  indent i 2 <> pack "</table>\n" <>
  indent i 1 <> pack "</body>\n" <>
  pack "</html>\n"

--------------------------------------------------------------------------------

jsonMissingValue :: MissingValue
jsonMissingValue = MissingValue $ pack "null"

jsonOptions :: StaticOptions
jsonOptions = StaticOptions jsonMissingValue Floating

jsonMeasurement :: Denominator -> Measurement -> Text
jsonMeasurement d (Measurement mt t u) =
  let
    colon = pack ": "
  in
    columnName JSON u <> colon <> formatToken t jsonOptions d mt

jsonModel :: DynamicOptions -> Model -> Text
jsonModel (DynamicOptions (HideModelName q) d _ _) (Model n xs) =
  let
    lbrace = pack "{ "
    comma' = linebreak <> pack "  , "
    rbrace = linebreak <> pack "  }" <> linebreak
    modelName (Name n')
      | q         = empty
      | otherwise = pack "\"model\": " <> quote n' <> comma'
  in
    lbrace <>
    modelName n <>
    intercalate' comma' (N.map (jsonMeasurement d) xs) <>
    rbrace

json :: OutputProcessor
json o@(DynamicOptions _ _ _ _) rs =
  let
    lbracket = pack "[ "
    comma    = pack ", "
    rbracket = pack "]"
  in
    lbracket <> intercalate' comma (N.map (jsonModel o) rs) <> rbracket

--------------------------------------------------------------------------------

latexSeparator :: Text
latexSeparator = pack " & "

latexNewLine :: Text
latexNewLine = pack "\\\\\n"

latexDelimiters :: Indentation -> Delimiters
latexDelimiters i = Delimiters (indent i 2) latexSeparator latexNewLine

latexOptions :: StaticOptions
latexOptions = StaticOptions missingValue Fixed

latex :: OutputProcessor
latex o@(DynamicOptions q@(HideModelName q') _ _ i) rs@(r N.:| _) =
  let
    hs = getHeadings Latex q r
    n  = length' hs
    (k, l) = if q' then (n, empty) else (n - 1, pack "l")
  in
    pack "\\documentclass{standalone}\n\\usepackage{standalone}\n" <>
    pack "\\begin{document}\n" <>
    indent i 1 <> pack "\\begin{tabular}{" <>
    l <> k `times` " r" <> pack "}\n" <>
    formatHeader (latexDelimiters i) hs <>
    indent i 2 <> pack "\\hline\n" <>
    formatContent latexOptions o (latexDelimiters i) getName rs <>
    indent i 1 <> pack "\\end{tabular}\n\\end{document}"

--------------------------------------------------------------------------------

mdDelimiters :: Delimiters
mdDelimiters = Delimiters (pack "| ") (pack " | ") (pack " |\n")

mdOptions :: StaticOptions
mdOptions = StaticOptions missingValue Fixed

markdown :: OutputProcessor
markdown o@(DynamicOptions q _ _ _) rs@(r N.:| _) =
  let
    hs = getHeadings Markdown q r
  in
    formatHeader mdDelimiters hs <>
    pack "| :- " <> ((length' hs - 1) `times` "| -: ") <> pack "|\n" <>
    formatContent mdOptions o mdDelimiters getName rs

--------------------------------------------------------------------------------

sqlOptions :: StaticOptions
sqlOptions = StaticOptions jsonMissingValue Floating

sqlHeader :: Text
sqlHeader = pack "create table if not exists spinResults(model nvarchar"

sqlDelimiters :: Delimiters
sqlDelimiters = Delimiters
  (pack "\ninsert into spinResults values(")
  (pack ", ")
  (pack ");")

sqlColumnDef :: Heading -> ValueType -> Text
sqlColumnDef h t = pack ", " <> h <> pack " " <> columnType t

getType :: Measurement -> ValueType
getType (Measurement _ t _) = t

mapReduce :: (a -> Text) -> N.NonEmpty a -> Text
mapReduce f xs = concat . N.toList . N.map f $ xs

sqlite :: OutputProcessor
sqlite o@(DynamicOptions q _ _ _) rs@(r N.:| _) =
  sqlHeader <>
  mapReduce (uncurry sqlColumnDef) (hs `N.zip` ts) <>
  pack ");" <>
  formatContent sqlOptions o sqlDelimiters getName rs'
  where hs  = case getHeadings SQLite q r of Headings xs -> xs
        ts  = N.map getType (getMeasurements r)
        -- Quick 'n dirty fix:
        rs' = N.map (\(Model n ms) -> Model (Name . quote $ getName n) ms) rs
        
--------------------------------------------------------------------------------

xmlHeader :: Text
xmlHeader = pack "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
                 \<!DOCTYPE spin-results SYSTEM \"spin-results.dtd\">\n\
                 \<spin-results xml:lang=\"en\">\n"

xmlFooter :: Text
xmlFooter = pack "</spin-results>"

xmlLeft1 :: Text
xmlLeft1 = pack "<"

xmlLeft2 :: Text
xmlLeft2 = pack "</"

xmlRight :: Text
xmlRight = pack ">"

xmlTag1 :: Text -> Text
xmlTag1 t = xmlLeft1 <> t <> xmlRight <> linebreak

xmlTag2 :: Text -> Text
xmlTag2 t = xmlLeft2 <> t <> xmlRight <> linebreak

xmlOptions :: StaticOptions
xmlOptions = StaticOptions missingValue Floating

xmlMeasurement :: DynamicOptions -> Measurement -> Text
xmlMeasurement (DynamicOptions _ d _ i) (Measurement mt t u) =
  indent i 2 <>
  pack "<measurement type=\"" <> tag <> pack "\">" <>
  formatToken t xmlOptions d mt <>
  pack "</measurement>\n"
  where tag = columnName XML u

xmlModel :: DynamicOptions -> Model -> Text
xmlModel o@(DynamicOptions (HideModelName q') _ _ i) (Model (Name n) xs) =
  let
    tag = pack "model"
    open
      | q'        = xmlTag1 tag
      | otherwise = xmlTag1 (tag <> pack " name=\"" <> n <> quotationMark)
    close = xmlTag2 tag
  in
    indent i 1 <> open <>
    foldMap (xmlMeasurement o) (N.toList xs) <>
    indent i 1 <> close

xml :: OutputProcessor
xml o rs = xmlHeader <> foldMap (xmlModel o) rs <> xmlFooter

--------------------------------------------------------------------------------

output :: Output -> Text
output (Output _        _ []    ) = empty
output (Output Latex    o (m:ms)) = latex    o (m N.:| ms)
output (Output HTML     o (m:ms)) = html     o (m N.:| ms)
output (Output Markdown o (m:ms)) = markdown o (m N.:| ms)
output (Output XML      o (m:ms)) = xml      o (m N.:| ms)
output (Output JSON     o (m:ms)) = json     o (m N.:| ms)
output (Output CSV      o (m:ms)) = csv      o (m N.:| ms)
output (Output Haskell  _ (m:ms)) = pack . show $ (m:ms)
output (Output SQLite   o (m:ms)) = sqlite   o (m N.:| ms)
