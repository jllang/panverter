module Panverter.Model where

import Data.List
import Data.List.NonEmpty hiding (map)
import Data.Text.Lazy     hiding (intercalate, map)

data TokenType    = Incomplete
                  | NeverClaim
                  | StateVector
                  | Depth
                  | Errors
                  | States
                  | StatesVisited
                  | StatesMatched
                  | Transitions
                  | AtomicSteps
                  | Conflicts
                  | EquivMemory
                  | ActualMemory
                  | Compression
                  | SVStored
                  | SVOverhead
                  | HashMemory
                  | StackMemory
                  | OtherMemory
                  | TotalMemory
                  | ElapsedTime
                  | Rate
                  -- | Wall clock time, as reported by '/usr/bin/time -v'
                  | TimeWallClock
                  -- | Amount of memory used, as reported by '/usr/bin/time -v'
                  | TimeMemory
                  deriving (Show, Read)
data ValueType    = Boolean
                  | Identifier
                  | FixedNumber
                  | FloatingNumber
                  | Megabytes
                  | Percentage
                  | Time
                  deriving (Eq, Ord, Show, Read)
newtype Name      = Name {getName :: Text} deriving (Show, Read)
data Measurement  = Measurement (Maybe Text) ValueType TokenType
                  deriving (Show, Read)
type Measurements = NonEmpty Measurement
data Model        = Model
                  { getModelName    :: Name
                  , getMeasurements :: Measurements
                  } deriving Read

-- Note that reading this output requires the GHC language extension
-- 'OverloadedStrings':
instance Show Model where
  show (Model n (m :| ms)) =
    let
      s    = "\n                          , "
    in
      " Model { getModelName = "     ++ show n  ++ "\n\
      \        , getMeasurements = " ++ show m  ++ " :|\n\
      \                          [ " ++ intercalate s (map show ms) ++ "\n\
      \                          ]\n\
      \        }\n"
