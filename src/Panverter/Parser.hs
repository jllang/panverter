module Panverter.Parser (parse) where

import Control.Monad
import Control.Monad.Trans.State.Lazy
import Data.Array
import qualified Data.List.NonEmpty as N
import qualified Data.Map.Lazy      as M
import qualified Data.Text.Lazy     as T
import Text.Regex.TDFA
import Text.Regex.TDFA.Text ()

import Panverter.Datatypes
import Panverter.Model
import Panverter.Token

preProcessor :: Token -> State ([Match], Rest) ()
preProcessor (Token r _ _) = do
  (ms, Rest s) <- get
  case matchOnceText r s of
    Just (_, a, s') -> do
      put (Match (fst (a ! 0)) : ms, Rest s')
    Nothing -> do
      put (Match empty : ms, Rest s)

preprocess :: Tokens -> T.Text -> N.NonEmpty (Match, Token)
preprocess (t N.:| ts) s =
  let
    ys = t:ts
    xs = fst $ execState (forM_ ys preProcessor) ([], Rest s)
  in
    N.fromList (reverse xs `zip` ys)

postprocess :: (Match, Token) -> Measurement
postprocess (Match s, Token _ t u) =
  let
    g = matchOnceText (postProcessors M.! t)
    f (_, a, _) = Just . fst $ a ! 0
  in
    Measurement (g s >>= f) t u

parse :: Tokens -> T.Text -> Measurements
parse ts = N.map postprocess . preprocess ts
