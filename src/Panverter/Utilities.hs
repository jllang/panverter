module Panverter.Utilities where

import Prelude   hiding (length, map, zip)
import Data.List.NonEmpty
import Data.Ord

import Panverter.Datatypes

-- | @x ==> f@ applies 'f' to 'x'.
(=>>) :: a -> (a -> b) -> b
(=>>) = flip ($)

-- | @f >>> g@ stands for @g . f@.
(>>>) :: (a -> b) -> (b -> c) -> (a -> c)
(>>>) = flip (.)

infixl 1 =>>
infixl 2 >>>

-- | Similar to 'Data.List.intercalate'.
intercalate' :: Monoid m => m -> NonEmpty m -> m
intercalate' _ (y :| [])     = y
intercalate' x (y :| (z:zs)) = y <> x <> intercalate' x (z :| zs)

-- | Given the sorted list 'is' of non-duplicated non-negative indices and a
--   list 'xs', this function returns the sublist of 'xs' containing only
--   elements with indices in 'is'.
filterIndices :: [Int] -> [a] -> [a]
filterIndices =
  let
    f _ _  [] = []
    f _ [] _  = []
    f j (i:is) (x:xs)
      | i == j    = x : f (j + 1) is     xs
      | otherwise =     f (j + 1) (i:is) xs
  in
    f 0

-- | Compresses a permutation. This means that the range for the second
--   components will be mapped to @[n - 1, n - 2 .. 0]@, where 'n' is the
--   length of the permutation, in a way that preserves the ordering in both
--   components. The permutation is assumed to be ordered according to the first
--   components. For example, @[(0,9),(1,8),(2,3),(3,6),(4,4)]@ will be
--   compressed into @[(0,4),(1,3),(2,0),(3,2),(4,1)]@.
compress :: Permutation -> Permutation
compress =
  sortBy (comparing snd) >>>
  (\xs -> let n = length xs in
    map fst xs `zip` fromList [n - 1, n - 2 .. 0]) >>>
  sortBy (comparing fst)

-- | Given the list 'p' representing a permutation of a list of 'n' values and
--   a list 'xs' with @length xs == n@, this function permutes 'xs' according to
--   'p'. This means that for each pair @(i,j)@ of indices in 'p', the element
--   at index 'i' will be moved to index 'j' in 'xs'. As a permutation, 'p' must
--   contain all indices @[0..n-1]@ exactly once in each component 'i' and 'j'
--   of every element @(i,j)@ in 'p' and be ordered with respect to the 'i'
--   components.
permute :: Permutation -> NonEmpty a -> NonEmpty a
permute p = zip p >>> sortBy (comparing (snd . fst)) >>> map snd
