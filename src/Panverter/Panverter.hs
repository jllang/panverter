module Panverter.Panverter (run) where

import Control.Monad
import Data.List
import Data.Maybe
import Database.SQLite.Simple
import qualified Data.Text.Lazy     as T
import qualified Data.Text.Lazy.IO  as TIO
import Options.Applicative
import System.Directory

import Panverter.Arguments
import Panverter.Datatypes
import Panverter.Model
import Panverter.Output
import Panverter.Parser
import Panverter.Token
import Panverter.Utilities

processFile :: Tokens -> Permutation -> FilePath -> IO (Maybe Model)
processFile ts p f = do
  b <- doesFileExist f
  case b of
    True -> do
      s <- TIO.readFile f
      let ys = parse ts s
      let xs = permute p ys
      return . return $ Model (Name . T.pack $ f) xs
    False -> return Nothing

compareModels :: Model -> Model -> Ordering
compareModels (Model (Name n) _) (Model (Name n') _) = compare n n'

dynamicOptions :: Arguments -> DynamicOptions
dynamicOptions (Arguments _ _ e f s i q _) =
    DynamicOptions
    (HideModelName q)
    (Denominator (10 ** fromIntegral e))
    (stylesheet f s)
    (Indentation i)
    where stylesheet HTML xs = Just (Stylesheet xs)
          stylesheet _    _  = Nothing

write :: Format -> FilePath -> T.Text -> IO ()
write SQLite f t = do
  let f' = if null f then "results.db" else f
  c <- open f'
  let qs = map (Query . T.toStrict) (T.lines t)
  forM_ qs (execute_ c)
  close c
write _      f t = do
  case f of
    [] -> TIO.putStrLn t
    _  -> TIO.writeFile f t

run :: IO ()
run = do
  a  <- execParser argumentsParser
  let (ts, p) = compileTokens . getTokenWord $ a
  fs <- getDirectoryContents (getDirectory a)
  ms <- forM fs (processFile ts p)
  let ms' = sortBy compareModels . catMaybes $ ms
  let o   = dynamicOptions a
  let f   = getOutputFile a
  let t   = output $ Output (getFormat a) o ms'
  write (getFormat a) f t
