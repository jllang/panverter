module Panverter.Arguments (argumentsParser) where

import Data.Char
import Data.List
import Options.Applicative

import Panverter.Datatypes
import Panverter.Token
import Panverter.Version

arguments :: Parser Arguments
arguments = Arguments
  <$> strOption
      (   long "directory"
       <> short 'd'
       <> help "Input directory; all its files will be treated as input"
       <> showDefault
       <> value "."
       <> metavar "DIR"
      )
  <*> strOption
      (   long "output-file"
       <> short 'o'
       <> help "Output file. If empty or unspecified, then the output will go \
               \to stdout, unless the output format is SQLite. An existing file\
               \ will be overwritten. With SQLite output format, the default \
               \output file is results.db."
       <> showDefault
       <> value ""
       <> metavar "FILE"
      )
  <*> option auto
      (   long "exponent"
       <> short 'e'
       <> help "Floating point values (depth, states, matched states, \
               \transitions, and atomic steps) will be divided by 10^N"
       <> showDefault
       <> value 6
       <> metavar "N"
      )
  <*> option readFormat
      (   long "format"
       <> short 'f'
       <> help "Output format (case insensitive); one of: CSV, Haskell \
               \(shorthand: hs), HTML, JSON, LaTeX, Markdown (shorthand: MD), \
               \SQLite, or XML. \
               \For the XML and Haskell output formats, see the files \
               \'spin-results.dtd' and 'src/Model.hs' respectively"
       <> showDefault
       <> value Latex
       <> metavar "FMT"
      )
  <*> strOption
      (   long "stylesheet"
       <> short 's'
       <> help "Applies only when used with '-f html'. If the value is a \
               \non-empty string, then it will be interpreted as an URL \
               \pointing to a CSS file. This stylesheet will be linked to the \
               \generated HTML document. If the argument is empty, then no \
               \stylesheet will be used"
       <> value ""
       <> metavar "URL"
      )
  <*> option auto
      (   long "indentation"
       <> short 'i'
       <> help "Amount of indentation in spaces to use with LaTeX, JSON, HTML, \
               \and XML output formats"
       <> showDefault
       <> value 2
       <> metavar "I"
      )
  <*> switch
      (   long "hide-model-name"
       <> short 'q'
       <> help "Hide the model name column. Could be useful if the results can \
               \be uniquely identified by the never claim, for example"
      )
  <*> option readTokens
      (   long "tokens"
       <> short 't'
       <> help "The tokens to be parsed from the input files. TOKENS is a \
               \non-empty string of symbols k, n, v, d, e, s, i, z, t, a, c, q,\
               \b, g, x, y, h, f, o, l, p, r, w, and m. \
               \The meanings of these values are the following: \
               \k := incompleteness; \
               \n := never claim; \
               \v := state-vector; \
               \d := depth reached; \
               \e := errors; \
               \s := states, stored; \
               \i := states, visited; \
               \z := states, matched; \
               \t := transitions; \
               \a := atomic steps; \
               \c := hash conflicts; \
               \q := equivalent memory usage for states; \
               \b := actual memory usage for states; \
               \g := compression ratio; \
               \x := state vector as stored; \
               \y := state vector overhead; \
               \h := memory used for hash table; \
               \f := memory used for DFS stack; \
               \o := other memory usage; \
               \l := total actual memory usage; \
               \p := elapsed time; \
               \r := states per second; \
               \w := wall clock time; \
               \m := maximum resident size. \
               \Only the first occurrence of each possible duplicated symbol \
               \has effect. After removing possible duplicates from TOKENS, \
               \Panverter converts it into a permutation that determines the \
               \order of columns or fields in the output. The order of the \
               \symbols and tokens above determine the identity permutation, \
               \i.e. \"nvdesiztacqbgxyhfolprwm\". \
               \For example, \"nvst\" prints the columns \"never claim\", \
               \\"state-vector\", \"states, stored\", and \"transitions\" in \
               \that order, while \"svnt\" swaps the columns \"state-vector\" \
               \and \"states, stored\". Similarly, \"tsvn\" reverses the order \
               \of columns. NB: \"vstn\" rotates the columns to the right \
               \rather than to the left, because it stands for the permutation \
               \(vstn), i.e. the set {(n,v),(v,s),(s,t),(t,n)}. The string \
               \\"all\" is a special alias for \"knvdesiztacqbgxyhfolprwm\""
       <> showDefault
       <> value "kestwm"
       <> metavar "TOKENS"
      )

readFormat :: ReadM Format
readFormat = str >>= \s -> case map toLower s of
  "csv"      -> return CSV
  "haskell"  -> return Haskell
  "hs"       -> return Haskell
  "html"     -> return HTML
  "json"     -> return JSON
  "latex"    -> return Latex
  "markdown" -> return Markdown
  "md"       -> return Markdown
  "sqlite"   -> return SQLite
  "xml"      -> return XML
  _          -> readerError $ "invalid or unsupported format " ++ s

readTokens :: ReadM String
readTokens = do
  s <- str
  let s' = nub s
  let valid = all (`elem` tokenAlphabet)
  case s of
    "all"        -> return tokenAlphabet
    _ | valid s' -> return s'
    _            -> readerError $ "invalid token word " ++ s

argumentsParser :: ParserInfo Arguments
argumentsParser = info
  (    helper
   <*> infoOption version
       (   long "version"
        <> short 'v'
        <> help "Prints the version number and exits"
       )
   <*> arguments
  )
  (   fullDesc
   <> progDesc "Converts the output of the verifier executable (pan) generated \
               \by the model checker Spin into CSS, Haskell, HTML, JSON, LaTeX,\
               \ markdown, SQLite, or XML."
   <> header ("Panverter - An output converter for the Spin model checker \
              \(version " ++ version ++ ")")
  )
