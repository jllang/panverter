module Panverter.Token
  ( tokenAlphabet
  , compileTokens
  , postProcessors
  , columnName
  , columnType
  ) where

import Prelude hiding (null)
import Data.List
import qualified Data.List.NonEmpty as N
import qualified Data.Map.Lazy      as M
import Data.Text.Lazy hiding (concat, map, zip)
import Text.Regex.TDFA
import Text.Regex.TDFA.Text ()

import Panverter.Datatypes
import Panverter.Model
import Panverter.Utilities

tIdentifier :: Text
tIdentifier = pack "[A-Za-z0-9\\.-_]+"

tFixedNumber :: Text
tFixedNumber = pack "[0-9]+"

tFloatingNumber :: Text
tFloatingNumber = pack "([0-9]+|[0-9]\\.[0-9]+e\\+[0-9]+)"

tMegabytes :: Text
tMegabytes = pack "[0-9]+\\.[0-9]{3}"

tTime :: Text
tTime = pack "([0-9]+(:[0-5][0-9]){2}|\
            \[0-5]?[0-9]:[0-5][0-9]\\.[0-9]{2})"

tPercentage :: Text
tPercentage = pack "[0-9]+\\.[0-9]{2}"

-- 'Boolean' should accept any non-empty string.
rBoolean :: Regex
rBoolean = makeRegex tIdentifier

rIdentifier :: Regex
rIdentifier = makeRegex tIdentifier

rFixedNumber :: Regex
rFixedNumber = makeRegex tFixedNumber

rFloatingNumber :: Regex
rFloatingNumber = makeRegex tFloatingNumber

rMegabytes :: Regex
rMegabytes = makeRegex tMegabytes

rTime :: Regex
rTime = makeRegex tTime

rPercentage :: Regex
rPercentage = makeRegex tPercentage

rIncomplete :: Regex
rIncomplete = makeRegex
              ( pack "(Warning: Search not completed|" <>
                pack "spin: " <> tIdentifier <> pack ":[0-9]+, Error:|" <>
                pack "pan: wrote " <> tIdentifier <> pack ".trail)" )

rNeverClaim :: Regex
rNeverClaim =
  makeRegex (pack "(- \\(none specified\\)|\\+ \\(" <> tIdentifier <> pack "\\))")

rStateVector :: Regex
rStateVector = makeRegex (pack "State-vector [0-9]+" <> tFloatingNumber)

rDepth :: Regex
rDepth = makeRegex (pack "depth reached " <> tFloatingNumber)

rErrors :: Regex
rErrors = makeRegex (pack "errors: " <> tFixedNumber)

rStates :: Regex
rStates = makeRegex (tFloatingNumber <> pack " states, stored")

rStatesVisited :: Regex
rStatesVisited = makeRegex (tFloatingNumber <> pack " visited")

rStatesMatched :: Regex
rStatesMatched = makeRegex (tFloatingNumber <> pack " states, matched")

rTransitions :: Regex
rTransitions = makeRegex (tFloatingNumber <> pack " transitions")

rAtomicSteps :: Regex
rAtomicSteps = makeRegex (tFloatingNumber <> pack " atomic steps")

rConflicts :: Regex
rConflicts = makeRegex (tFloatingNumber <> pack " (resolved)")

rEquivMem :: Regex
rEquivMem = makeRegex (tMegabytes <> pack "[[:space:]]+equivalent memory usage for states")

rActualMem :: Regex
rActualMem = makeRegex (tMegabytes <> pack "[[:space:]]+actual memory usage for states")

rCompression :: Regex
rCompression = makeRegex (pack "compression: " <> tPercentage <> pack "%")

rSVStored :: Regex
rSVStored = makeRegex (pack "state-vector as stored = " <> tFixedNumber)

rSVOverhead :: Regex
rSVOverhead = makeRegex (tFixedNumber <> pack " byte overhead")

rHashMem :: Regex
rHashMem = makeRegex (tMegabytes <> pack "[[:space:]]+memory used for hash table")

rStackMem :: Regex
rStackMem = makeRegex (tMegabytes <> pack "[[:space:]]+memory used for DFS stack")

rOtherMem :: Regex
rOtherMem = makeRegex (tMegabytes <> pack "[[:space:]]+other")

rTotalMem :: Regex
rTotalMem = makeRegex (tMegabytes <> pack "[[:space:]]+total actual memory usage")

rElapsedTime :: Regex
rElapsedTime = makeRegex (pack "elapsed time " <> tFloatingNumber)

rRate :: Regex
rRate = makeRegex (pack "rate " <> tFloatingNumber)

rWallClock :: Regex
rWallClock =
  makeRegex (pack "Elapsed \\(wall clock\\) time \\(h:mm:ss or m:ss\\): " <>
  tTime)

rMemory :: Regex
rMemory = makeRegex (pack "Maximum resident set size \\(kbytes\\): [0-9]+")

positions :: [(Char,Int)]
positions = [ ('k',  0) , ('n',  1) , ('v',  2) , ('d',  3) , ('e',  4)
            , ('s',  5) , ('i',  6) , ('z',  7) , ('t',  8) , ('a',  9)
            , ('c', 10) , ('q', 11) , ('b', 12) , ('g', 13) , ('x', 14)
            , ('y', 15) , ('h', 16) , ('f', 17) , ('o', 18) , ('l', 19)
            , ('p', 20) , ('r', 21) , ('w', 22) , ('m', 23)
            ]

positionMap :: M.Map Char Int
positionMap = M.fromList positions

tokens :: [Token]
tokens = [ Token rIncomplete    Boolean        Incomplete
         , Token rNeverClaim    Identifier     NeverClaim
         , Token rStateVector   FixedNumber    StateVector
         , Token rDepth         FloatingNumber Depth
         , Token rErrors        FixedNumber    Errors
         , Token rStates        FloatingNumber States
         , Token rStatesVisited FloatingNumber StatesVisited
         , Token rStatesMatched FloatingNumber StatesMatched
         , Token rTransitions   FloatingNumber Transitions
         , Token rAtomicSteps   FloatingNumber AtomicSteps
         , Token rConflicts     FloatingNumber Conflicts
         , Token rEquivMem      Megabytes      EquivMemory
         , Token rActualMem     Megabytes      ActualMemory
         , Token rCompression   Percentage     Compression
         , Token rSVStored      FixedNumber    SVStored
         , Token rSVOverhead    FixedNumber    SVOverhead
         , Token rHashMem       Megabytes      HashMemory
         , Token rStackMem      Megabytes      StackMemory
         , Token rOtherMem      Megabytes      OtherMemory
         , Token rTotalMem      Megabytes      TotalMemory
         , Token rElapsedTime   FloatingNumber ElapsedTime
         , Token rRate          FloatingNumber Rate
         , Token rWallClock     Time           TimeWallClock
         , Token rMemory        FixedNumber    TimeMemory
         ]

tsIntro :: (Char, Int) -> TokenSymbol
tsIntro (c, p) = TokenSymbol c (Position p)

tsElimR :: TokenSymbol -> Int
tsElimR         (TokenSymbol _ (Position p)) = p

tokenMap :: M.Map TokenSymbol Token
tokenMap = M.fromList (map tsIntro positions `zip` tokens)

compileTokens :: String -> (Tokens, Permutation)
compileTokens s =
  let
    failure = error $ "invalid token word " ++ s
    w  = s =>> map (positionMap M.!?)
           >>> sequence
           >>> maybe failure (map tsIntro . zip s)
    ts = w =>> sort
           >>> map (tokenMap M.!?)
           >>> sequence
           >>> maybe failure N.fromList
    p  = w =>> map tsElimR
           >>> zip [0..]
           >>> N.fromList
  in
    (ts, p)

tokenAlphabet :: String
tokenAlphabet = map fst $ positions

--------------------------------------------------------------------------------

postProcessors :: M.Map ValueType Regex
postProcessors = M.fromList
               [ ( Boolean        , rBoolean        )
               , ( Identifier     , rIdentifier     )
               , ( FixedNumber    , rFixedNumber    )
               , ( FloatingNumber , rFloatingNumber )
               , ( Megabytes      , rMegabytes      )
               , ( Percentage     , rPercentage     )
               , ( Time           , rTime           )
               ]

-- LaTeX, HTML, markdown:
columns1 :: TokenType -> Text
columns1 t = case t of
  Incomplete    -> pack "Incomplete"
  NeverClaim    -> pack "Never Claim"
  StateVector   -> pack "State-vector"
  Depth         -> pack "Depth"
  Errors        -> pack "Errors"
  States        -> pack "States"
  StatesVisited -> pack "States, Visited"
  StatesMatched -> pack "States, Matched"
  Transitions   -> pack "Transitions"
  AtomicSteps   -> pack "Atomic Steps"
  Conflicts     -> pack "Hash Conflicts"
  EquivMemory   -> pack "Equiv. Memory"
  ActualMemory  -> pack "Actual Memory"
  Compression   -> pack "Compression"
  SVStored      -> pack "State Vect., Stored"
  SVOverhead    -> pack "State Vect., Overhead"
  HashMemory    -> pack "Hash Memory"
  StackMemory   -> pack "DFS Stack Memory"
  OtherMemory   -> pack "Other Memory"
  TotalMemory   -> pack "Total Memory"
  ElapsedTime   -> pack "Elapsed Time"
  Rate          -> pack "States per Second"
  TimeWallClock -> pack "Wall Clock"
  TimeMemory    -> pack "Memory"

-- XML:
columns2 :: TokenType -> Text
columns2 t = case t of
  Incomplete    -> pack "incomplete"
  NeverClaim    -> pack "never-claim"
  StateVector   -> pack "state-vector"
  Depth         -> pack "depth"
  Errors        -> pack "errors"
  States        -> pack "states-stored"
  StatesVisited -> pack "states-visited"
  StatesMatched -> pack "states-matched"
  Transitions   -> pack "transitions"
  AtomicSteps   -> pack "atomic-steps"
  Conflicts     -> pack "hash-conflicts"
  EquivMemory   -> pack "equiv-memory"
  ActualMemory  -> pack "actual-memory"
  Compression   -> pack "compression"
  SVStored      -> pack "state-vect-stored"
  SVOverhead    -> pack "state-vect-overhead"
  HashMemory    -> pack "hash-memory"
  StackMemory   -> pack "dfs-stack-memory"
  OtherMemory   -> pack "other-memory"
  TotalMemory   -> pack "total-memory"
  ElapsedTime   -> pack "elapsed-time"
  Rate          -> pack "states-per-second"
  TimeWallClock -> pack "wall-clock"
  TimeMemory    -> pack "memory"

-- SQLite:
columns3 :: TokenType -> Text
columns3 t = case t of
  Incomplete    -> pack "incomplete"
  NeverClaim    -> pack "neverClaim"
  StateVector   -> pack "stateVector"
  Depth         -> pack "depth"
  Errors        -> pack "errors"
  States        -> pack "statesStored"
  StatesVisited -> pack "statesVisited"
  StatesMatched -> pack "statesMatched"
  Transitions   -> pack "transitions"
  AtomicSteps   -> pack "atomicSteps"
  Conflicts     -> pack "hashConflicts"
  EquivMemory   -> pack "equivMemory"
  ActualMemory  -> pack "actualMemory"
  Compression   -> pack "compression"
  SVStored      -> pack "stateVectStored"
  SVOverhead    -> pack "stateVectOverhead"
  HashMemory    -> pack "hashMemory"
  StackMemory   -> pack "dfsStackMemory"
  OtherMemory   -> pack "otherMemory"
  TotalMemory   -> pack "totalMemory"
  ElapsedTime   -> pack "elapsedTime"
  Rate          -> pack "statesPerSecond"
  TimeWallClock -> pack "wallClock"
  TimeMemory    -> pack "memory"
  
-- JSON:
columns4 :: TokenType -> Text
columns4 = pack . show . columns3

-- CSV:
columns5 :: TokenType -> Text
columns5 t = pack "\"" <> columns1 t <> pack "\""

columnName :: Format -> TokenType -> Text
columnName CSV    = columns5
columnName JSON   = columns4
columnName SQLite = columns3
columnName XML    = columns2
columnName _      = columns1

-- For SQLite:
columnType :: ValueType -> Text
columnType Boolean        = pack "integer"
columnType Identifier     = pack "nvarchar"
columnType FixedNumber    = pack "integer"
columnType FloatingNumber = pack "real"
columnType Megabytes      = pack "real"
columnType Percentage     = pack "real"
columnType Time           = pack "varchar"
